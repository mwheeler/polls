import random

from django.core.management.base import BaseCommand, CommandError

from polls.factories import ChoiceFactory, QuestionFactory
from polls.models import Question, Choice


class Command(BaseCommand):
    help = "Seeds empty DB with poll data"

    def add_arguments(self, parser):
        parser.add_argument("questions", nargs=1, type=int)

    def handle(self, *args, **options):
        number_of_questions = options["questions"][0]
        if not Question.objects.all().exists():
            questions = QuestionFactory.create_batch(number_of_questions)
            for question in questions:
                choice_count = random.choice(range(3, 6))
                ChoiceFactory.create_batch(choice_count, question=question)
            choices_created = Choice.objects.count()
            self.stdout.write(self.style.SUCCESS(
                f"{number_of_questions} question(s) and {choices_created} choices created"
            ))
        else:
            self.stdout.write(self.style.SUCCESS("DB already has questions. No items created."))
