import factory

import django.utils.timezone as tz

from . import models


class QuestionFactory(factory.DjangoModelFactory):
    """
    Factory for Question objects.
    """

    class Meta:
        model = models.Question

    question_text = factory.Faker('paragraph')
    pub_date = factory.Faker('past_datetime', tzinfo=tz.get_default_timezone())


class ChoiceFactory(factory.DjangoModelFactory):
    """
    Factory for Choice objects.
    """

    class Meta:
        model = models.Choice

    question = factory.SubFactory(QuestionFactory)
    choice_text = factory.Faker('sentence')
